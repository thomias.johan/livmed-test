import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { signIn, signOut, useSession } from "next-auth/react"
import Link from 'next/link'

export default function Home({ posts }) {
  const { data: session, status } = useSession()
  const loading = status === "loading"
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Create Next App" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="https://nextjs.org">Livmed Test !</a>
        </h1>

        {!session && (
          <>
            <span>
              Vous n êtes pas connecté
            </span>
            <a
              href={`/api/auth/signin`}
              className={styles.buttonPrimary}
              onClick={(e) => {
                e.preventDefault()
                signIn()
              }}
            >
              Se connecter
            </a>
          </>
        )}
   {session && (
     
          <>
          <span>
              Vous êtes connecté
            </span>
          
          {posts.map(post =>
          // eslint-disable-next-line react/jsx-key
          <Link href={`/blog/${post.id}`}>
            <a>
              <div className={styles.card}>
                <span>{post.id}</span>
                <h3>{post.title}</h3>
                <p>{post.body}</p>
              </div>
            </a>
          </Link>
        )
        }
          </>
          )}

      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}


export async function getStaticProps() {

  const posts = await fetch('https://jsonplaceholder.typicode.com/posts?_limit=4').then(reponse => reponse.json())
  return {
    props: {
      posts
    }
  }
}

