import styles from '../../styles/Home.module.css'
export default function Post ({post, comment}) {
    return (
    
        <main className={styles.main}>
            <h1 className={styles.title}>{post.title}</h1>
            <p className={styles.text}>{post.body}</p>
       
            <h2>Commentaires</h2>
            {comment.map(comment =>
            // eslint-disable-next-line react/jsx-key
            <p className={styles.text}>{comment.email} <br/>{comment.body}</p>
            )}
        </main>


    )
}
export async function getStaticProps({params}){
    const post = await fetch(`https://jsonplaceholder.typicode.com/posts/${params.id}`).then(reponse => reponse.json())
    const comment = await fetch(`https://jsonplaceholder.typicode.com/posts/${params.id}/comments`).then(reponse => reponse.json())
    return {
        props: {
            post,
            comment
        }
    }
}

export async function getStaticPaths(){
 
    const posts = await fetch('https://jsonplaceholder.typicode.com/posts?_limit=4').then(reponse => reponse.json())
    return {
     paths: posts.map(post => ({
       params: {id: post.id.toString()}
     })),
     fallback: false,
    }
  }

